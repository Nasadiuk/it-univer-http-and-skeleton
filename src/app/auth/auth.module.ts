import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import {AuthRouting} from './auth.routing';
import {AppSharedModule} from '../shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule, AuthRouting, AppSharedModule
  ],
  declarations: [AuthComponent],
  providers: []
})
export class AuthModule { }
